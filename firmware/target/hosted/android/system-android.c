/***************************************************************************
 *             __________               __   ___.
 *   Open      \______   \ ____   ____ |  | _\_ |__   _______  ___
 *   Source     |       _//  _ \_/ ___\|  |/ /| __ \ /  _ \  \/  /
 *   Jukebox    |    |   (  <_> )  \___|    < | \_\ (  <_> > <  <
 *   Firmware   |____|_  /\____/ \___  >__|_ \|___  /\____/__/\_ \
 *                     \/            \/     \/    \/            \/
 * $Id$
 *
 * Copyright (c) 2010 Thomas Martitz
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY
 * KIND, either express or implied.
 *
 ****************************************************************************/


#include <setjmp.h>
//#include <jni.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/reboot.h>
#include <sys/stat.h>
#include <stdio.h>
#include "config.h"
#include "system.h"
#include "power.h"
#include "button.h"


uintptr_t *stackbegin;
uintptr_t *stackend;

extern int main(void);

void system_exception_wait(void)
{
	while(1);
}

void system_reboot(void)
{
	reboot(RB_AUTOBOOT);
}

void power_off(void)
{
	reboot(RB_POWER_OFF);
}


void system_init(void)
{
    FILE *f;
    
    /* no better place yet */
    volatile uintptr_t stack = 0;
    stackbegin = stackend = (uintptr_t*) &stack;

    struct stat m1, m2;
    stat("/mnt/", &m1);
    do
    {
        puts("waiting for storage to get mounted");
        stat("/sdcard/", &m2);
        usleep(100000);
    }
    while(m1.st_dev == m2.st_dev);
   
    /* Activate mute - needed at startup */
    f = fopen("/sys/class/codec/mute", "w");
    fprintf(f, "%s\n", "B");
    fclose(f); 

    /* Unmute */
    f = fopen("/sys/class/codec/wm8740_mute", "w");
    fputc(0, f);
    fclose(f);
}

int hostfs_init(void)
{
    /* stub */
    return 0;
}

int hostfs_flush(void)
{
    sync();
    return 0;
}


/* below is the facility for external (from other java threads) to safely call
 * into our snative code. When extracting rockbox.zip the main function is
 * called only after extraction. This delay can be accounted for by calling
 * wait_rockbox_ready(). This does not return until the critical parts of Rockbox
 * can be considered up and running. */
static pthread_cond_t btn_cond = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t btn_mtx = PTHREAD_MUTEX_INITIALIZER;
static bool initialized;

bool is_rockbox_ready(void)
{
    /* don't bother with mutexes for this */
    return initialized;
}

void wait_rockbox_ready(void)
{
    pthread_mutex_lock(&btn_mtx);

    if (!initialized)
        pthread_cond_wait(&btn_cond, &btn_mtx);

    pthread_mutex_unlock(&btn_mtx);
}

void set_rockbox_ready(void)
{
    pthread_mutex_lock(&btn_mtx);

    initialized = true;
    /* now ready. signal all waiters */
    pthread_cond_broadcast(&btn_cond);

    pthread_mutex_unlock(&btn_mtx);
}
