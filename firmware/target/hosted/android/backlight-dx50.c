/***************************************************************************
 *             __________               __   ___.
 *   Open      \______   \ ____   ____ |  | _\_ |__   _______  ___
 *   Source     |       _//  _ \_/ ___\|  |/ /| __ \ /  _ \  \/  /
 *   Jukebox    |    |   (  <_> )  \___|    < | \_\ (  <_> > <  <
 *   Firmware   |____|_  /\____/ \___  >__|_ \|___  /\____/__/\_ \
 *                     \/            \/     \/    \/            \/
 *
 * Copyright (C) 2011 by Lorenzo Miori
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY
 * KIND, either express or implied.
 *
 ****************************************************************************/
#include "config.h"
#include "system.h"
#include "backlight.h"
#include "backlight-target.h"
#include "lcd.h"
#include <fcntl.h>
#include <stdio.h>
#include "unistd.h"
#include "button-target.h"
#include "settings.h"

int get_last_btns(void);

bool _backlight_init(void)
{
    /* We have nothing to do */
    return true;
}

void _backlight_on(void)
{
    set_backlight_filter_keypress(global_settings.bl_filter_first_keypress);

    if(get_last_btns() & BUTTON_POWER)
    {
        /* Turn backlight on */
        FILE *f = fopen("/sys/class/backlight/rk28_bl/bl_power", "w");
        fprintf(f, "%d", 0);
        fclose(f);
    }

    backlight_state = true;
}

void _backlight_off(void)
{
    set_backlight_filter_keypress(global_settings.bl_filter_first_keypress);

    /* Turn backlight off only instead of putting system to sleep */    
        FILE *f = fopen("/sys/class/backlight/rk28_bl/bl_power", "w");
        fprintf(f, "%d", 1);
	fclose(f);

	backlight_state = false;
}

void _backlight_set_brightness(int brightness)
{
    /* Brightness value must not go below min and above max values */
    if (brightness > MAX_BRIGHTNESS_SETTING)
        brightness = MAX_BRIGHTNESS_SETTING;
    if (brightness < MIN_BRIGHTNESS_SETTING)
        brightness = MIN_BRIGHTNESS_SETTING;

	FILE *f = fopen("/sys/class/backlight/rk28_bl//brightness", "w");
	fprintf(f, "%d", brightness);
	fclose(f);
}
