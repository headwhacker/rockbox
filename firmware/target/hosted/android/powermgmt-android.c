/***************************************************************************
 *             __________               __   ___.
 *   Open      \______   \ ____   ____ |  | _\_ |__   _______  ___
 *   Source     |       _//  _ \_/ ___\|  |/ /| __ \ /  _ \  \/  /
 *   Jukebox    |    |   (  <_> )  \___|    < | \_\ (  <_> > <  <
 *   Firmware   |____|_  /\____/ \___  >__|_ \|___  /\____/__/\_ \
 *                     \/            \/     \/    \/            \/
 * $Id$
 *
 * Copyright (C) 2010 by Thomas Martitz
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY
 * KIND, either express or implied.
 *
 ****************************************************************************/


#include <stdbool.h>
#include <stdio.h>
#include <math.h>
#include "config.h"
#include "power.h" 

/* report battery level as percentage of remaining voltage */
int _battery_level(void)
{
        int voltage_max; 
        int voltage_min; 
        int voltage_now;
        int voltage_remain;
        int voltage_full;
        int battery_level;
        FILE *f;

        f = fopen("/sys/class/power_supply/battery/voltage_max_design", "r");
        fscanf(f, "%d", &voltage_max);
        fclose(f);
 
        f = fopen("/sys/class/power_supply/battery/voltage_min_design", "r");
        fscanf(f, "%d", &voltage_min);
        fclose(f);

        f = fopen("/sys/class/power_supply/battery/voltage_now", "r");
        fscanf(f, "%d", &voltage_now);
        fclose(f);

        voltage_now /= 10000;
        voltage_max /= 10;
        voltage_min /= 10;

        if (voltage_now < voltage_min)
	{
		voltage_now = voltage_min;
	}
        
	voltage_remain = (voltage_now - voltage_min) + 10;
        voltage_full = voltage_max - voltage_min;

        if (voltage_remain < voltage_full / 2)
	{
		voltage_remain = voltage_now - voltage_min;
        }

        if (voltage_remain > voltage_full) 
        {
		voltage_remain = voltage_full;
        }

        battery_level = voltage_remain * 100  / voltage_full;
	
	return battery_level;
}

unsigned int power_input_status(void)
{
    int val;
	FILE *f = fopen("/sys/class/power_supply/ac/present", "r");
	fscanf(f, "%d", &val);
	fclose(f);
    return val?POWER_INPUT_MAIN_CHARGER:POWER_INPUT_NONE;
}
 

