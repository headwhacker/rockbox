#             __________               __   ___.
#   Open      \______   \ ____   ____ |  | _\_ |__   _______  ___
#   Source     |       _//  _ \_/ ___\|  |/ /| __ \ /  _ \  \/  /
#   Jukebox    |    |   (  <_> )  \___|    < | \_\ (  <_> > <  <
#   Firmware   |____|_  /\____/ \___  >__|_ \|___  /\____/__/\_ \
#                     \/            \/     \/    \/            \/
# $Id$
#

LRCPLAYERSRCDIR := $(APPSDIR)/plugins/lrcplayer
LRCPLAYERBUILDDIR := $(BUILDDIR)/apps/plugins/lrcplayer

ROCKS += $(LRCPLAYERBUILDDIR)/lrcplayer.rock

LRCPLAYER_SRC := $(call preprocess, $(LRCPLAYERSRCDIR)/SOURCES)
LRCPLAYER_OBJ := $(call c2obj, $(LRCPLAYER_SRC))

# add source files to OTHER_SRC to get automatic dependencies
OTHER_SRC += $(LRCPLAYER_SRC)

LRCPLAYERFLAGS = $(filter-out -O%,$(PLUGINFLAGS)) -O3 -DFIXED_POINT=16

$(LRCPLAYERBUILDDIR)/lrcplayer.rock: $(LRCPLAYER_OBJ)

$(LRCPLAYERBUILDDIR)/%.o: $(LRCPLAYERSRCDIR)/%.c $(LRCPLAYERSRCDIR)/lrcplayer.make
	$(SILENT)mkdir -p $(dir $@)
	$(call PRINTS,CC $(subst $(ROOTDIR)/,,$<))$(CC) -I$(dir $<) $(LRCPLAYERFLAGS) -c $< -o $@
