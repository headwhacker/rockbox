#             __________               __   ___.
#   Open      \______   \ ____   ____ |  | _\_ |__   _______  ___
#   Source     |       _//  _ \_/ ___\|  |/ /| __ \ /  _ \  \/  /
#   Jukebox    |    |   (  <_> )  \___|    < | \_\ (  <_> > <  <
#   Firmware   |____|_  /\____/ \___  >__|_ \|___  /\____/__/\_ \
#                     \/            \/     \/    \/            \/
# $Id$
#

TEXT_EDITORSRCDIR := $(APPSDIR)/plugins/text_editor
TEXT_EDITORBUILDDIR := $(BUILDDIR)/apps/plugins/text_editor

ROCKS += $(TEXT_EDITORBUILDDIR)/text_editor.rock

TEXT_EDITOR_SRC := $(call preprocess, $(TEXT_EDITORSRCDIR)/SOURCES)
TEXT_EDITOR_OBJ := $(call c2obj, $(TEXT_EDITOR_SRC))

# add source files to OTHER_SRC to get automatic dependencies
OTHER_SRC += $(TEXT_EDITOR_SRC)

TEXT_EDITORFLAGS = $(filter-out -O%,$(PLUGINFLAGS)) -O3 -DFIXED_POINT=16

$(TEXT_EDITORBUILDDIR)/text_editor.rock: $(TEXT_EDITOR_OBJ)

$(TEXT_EDITORBUILDDIR)/%.o: $(TEXT_EDITORSRCDIR)/%.c $(TEXT_EDITORSRCDIR)/text_editor.make
	$(SILENT)mkdir -p $(dir $@)
	$(call PRINTS,CC $(subst $(ROOTDIR)/,,$<))$(CC) -I$(dir $<) $(TEXT_EDITORFLAGS) -c $< -o $@
