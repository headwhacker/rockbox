#             __________               __   ___.
#   Open      \______   \ ____   ____ |  | _\_ |__   _______  ___
#   Source     |       _//  _ \_/ ___\|  |/ /| __ \ /  _ \  \/  /
#   Jukebox    |    |   (  <_> )  \___|    < | \_\ (  <_> > <  <
#   Firmware   |____|_  /\____/ \___  >__|_ \|___  /\____/__/\_ \
#                     \/            \/     \/    \/            \/
# $Id$
#

OSCILLOSCOPESRCDIR := $(APPSDIR)/plugins/oscilloscope
OSCILLOSCOPEBUILDDIR := $(BUILDDIR)/apps/plugins/oscilloscope

ROCKS += $(OSCILLOSCOPEBUILDDIR)/oscilloscope.rock

OSCILLOSCOPE_SRC := $(call preprocess, $(OSCILLOSCOPESRCDIR)/SOURCES)
OSCILLOSCOPE_OBJ := $(call c2obj, $(OSCILLOSCOPE_SRC))

# add source files to OTHER_SRC to get automatic dependencies
OTHER_SRC += $(OSCILLOSCOPE_SRC)

OSCILLOSCOPEFLAGS = $(filter-out -O%,$(PLUGINFLAGS)) -O3 -DFIXED_POINT=16

$(OSCILLOSCOPEBUILDDIR)/oscilloscope.rock: $(OSCILLOSCOPE_OBJ)

$(OSCILLOSCOPEBUILDDIR)/%.o: $(OSCILLOSCOPESRCDIR)/%.c $(OSCILLOSCOPESRCDIR)/oscilloscope.make
	$(SILENT)mkdir -p $(dir $@)
	$(call PRINTS,CC $(subst $(ROOTDIR)/,,$<))$(CC) -I$(dir $<) $(OSCILLOSCOPEFLAGS) -c $< -o $@
