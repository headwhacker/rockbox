#             __________               __   ___.
#   Open      \______   \ ____   ____ |  | _\_ |__   _______  ___
#   Source     |       _//  _ \_/ ___\|  |/ /| __ \ /  _ \  \/  /
#   Jukebox    |    |   (  <_> )  \___|    < | \_\ (  <_> > <  <
#   Firmware   |____|_  /\____/ \___  >__|_ \|___  /\____/__/\_ \
#                     \/            \/     \/    \/            \/
# $Id$
#

TEXT_VIEWER_SRCDIR := $(APPSDIR)/plugins/text_viewer
TEXT_VIEWER_BUILDDIR := $(BUILDDIR)/apps/plugins/text_viewer

ROCKS += $(TEXT_VIEWER_BUILDDIR)/text_viewer.rock

TEXT_VIEWER_SRC := $(call preprocess, $(TEXT_VIEWER_SRCDIR)/SOURCES)
TEXT_VIEWER_OBJ := $(call c2obj, $(TEXT_VIEWER_SRC))

# add source files to OTHER_SRC to get automatic dependencies
OTHER_SRC += $(TEXT_VIEWER_SRC)

TEXT_VIEWERFLAGS = $(filter-out -O%,$(PLUGINFLAGS)) -O3 -DFIXED_POINT=16

$(TEXT_VIEWER_BUILDDIR)/text_viewer.rock: $(TEXT_VIEWER_OBJ)

$(TEXT_VIEWERBUILDDIR)/%.o: $(TEXT_VIEWERSRCDIR)/%.c $(TEXT_VIEWERSRCDIR)/text_viewer.make
	$(SILENT)mkdir -p $(dir $@)
	$(call PRINTS,CC $(subst $(ROOTDIR)/,,$<))$(CC) -I$(dir $<) $(TEXT_VIEWERFLAGS) -c $< -o $@
