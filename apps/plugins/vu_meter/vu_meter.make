#             __________               __   ___.
#   Open      \______   \ ____   ____ |  | _\_ |__   _______  ___
#   Source     |       _//  _ \_/ ___\|  |/ /| __ \ /  _ \  \/  /
#   Jukebox    |    |   (  <_> )  \___|    < | \_\ (  <_> > <  <
#   Firmware   |____|_  /\____/ \___  >__|_ \|___  /\____/__/\_ \
#                     \/            \/     \/    \/            \/
# $Id$
#

VU_METERSRCDIR := $(APPSDIR)/plugins/vu_meter
VU_METERBUILDDIR := $(BUILDDIR)/apps/plugins/vu_meter

ROCKS += $(VU_METERBUILDDIR)/vu_meter.rock

VU_METER_SRC := $(call preprocess, $(VU_METERSRCDIR)/SOURCES)
VU_METER_OBJ := $(call c2obj, $(VU_METER_SRC))

# add source files to OTHER_SRC to get automatic dependencies
OTHER_SRC += $(VU_METER_SRC)

VU_METERFLAGS = $(filter-out -O%,$(PLUGINFLAGS)) -O3 -DFIXED_POINT=16

$(VU_METERBUILDDIR)/vu_meter.rock: $(VU_METER_OBJ)

$(VU_METERBUILDDIR)/%.o: $(VU_METERSRCDIR)/%.c $(VU_METERSRCDIR)/vu_meter.make
	$(SILENT)mkdir -p $(dir $@)
	$(call PRINTS,CC $(subst $(ROOTDIR)/,,$<))$(CC) -I$(dir $<) $(VU_METERFLAGS) -c $< -o $@
